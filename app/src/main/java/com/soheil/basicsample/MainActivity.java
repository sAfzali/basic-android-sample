package com.soheil.basicsample;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.soheil.basicsample.utils.PublicMethods;

public class MainActivity extends AppCompatActivity {
    EditText userName, password, mobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();

    }

    void bind() {
        userName = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.show).setOnClickListener(V -> {
            onClick();
        });
        ((Button) findViewById(R.id.show)).setOnClickListener(V -> {
            onClick();
        });


    }

    private void onClick() {
        Toast.makeText(MainActivity.this, "msg", Toast.LENGTH_SHORT).show();
        PublicMethods.toast(MainActivity.this, "msg");
    }


}
