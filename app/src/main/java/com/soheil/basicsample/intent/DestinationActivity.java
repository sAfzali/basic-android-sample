package com.soheil.basicsample.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.soheil.basicsample.R;

public class DestinationActivity extends AppCompatActivity {
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        result = findViewById(R.id.result);
        String userValue = getIntent().getStringExtra("username");
        String passValue = getIntent().getStringExtra("password");
        result.setText(String.format("%s%s", userValue, passValue));
    }
}
