package com.soheil.basicsample.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.soheil.basicsample.R;

public class OriginActivity extends AppCompatActivity {
    EditText userName, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin);
        bind();
    }

    private void bind() {
        userName = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        findViewById(R.id.show).setOnClickListener(V -> {
            onClick(userName.getText().toString()
                    , password.getText().toString());
        });
    }

    private void onClick(String userValue, String passwordValue) {
        Intent intent = new Intent(OriginActivity.this, DestinationActivity.class);
        intent.putExtra("username", userValue);
        intent.putExtra("password", passwordValue);
        startActivity(intent);

    }
}
